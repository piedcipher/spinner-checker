package com.wordpress.piedcipher.spinnerchecker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinnerOne = findViewById(R.id.spinnerOne);
        Spinner spinnerTwo = findViewById(R.id.spinnerTwo);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.mock_spinner_data,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerOne.setAdapter(adapter);
        spinnerTwo.setAdapter(adapter);

        spinnerOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SpinnerCheckerData.setSpinnerOneSelectedItem(adapterView.getItemAtPosition(i).toString());
                if(SpinnerCheckerData.getSpinnerTwoSelectedItem() != null) {
                    if(SpinnerCheckerData.getSpinnerOneSelectedItem().equals(SpinnerCheckerData.getSpinnerTwoSelectedItem())) {
                        Toast.makeText(getApplicationContext(), "Equal", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SpinnerCheckerData.setSpinnerTwoSelectedItem(adapterView.getItemAtPosition(i).toString());
                if(SpinnerCheckerData.getSpinnerOneSelectedItem() != null) {
                    if(SpinnerCheckerData.getSpinnerTwoSelectedItem().equals(SpinnerCheckerData.getSpinnerOneSelectedItem())) {
                        Toast.makeText(getApplicationContext(), "Equal", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
