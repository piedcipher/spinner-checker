package com.wordpress.piedcipher.spinnerchecker;

public class SpinnerCheckerData {
    private static String spinnerOneSelectedItem;
    private static String spinnerTwoSelectedItem;

    public static String getSpinnerOneSelectedItem() {
        return spinnerOneSelectedItem;
    }

    public static String getSpinnerTwoSelectedItem() {
        return spinnerTwoSelectedItem;
    }

    public static void setSpinnerOneSelectedItem(String _spinnerOneSelectedItem) {
        spinnerOneSelectedItem = _spinnerOneSelectedItem;
    }

    public static void setSpinnerTwoSelectedItem(String _spinnerTwoSelectedItem) {
        spinnerTwoSelectedItem = _spinnerTwoSelectedItem;
    }
}